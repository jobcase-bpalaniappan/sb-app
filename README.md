# Springboot REST API

### Postgres Docker
* Run postgres in docker - `docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -p 5432:5432 -d --rm postgres:alpine`

### Testing SpringBoot
* Good Springboot testing [doc](https://www.baeldung.com/spring-boot-testing)
* Another good Spring boot unit and integration testing [doc](https://reflectoring.io/spring-boot-data-jpa-test/)