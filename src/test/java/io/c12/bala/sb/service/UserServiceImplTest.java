package io.c12.bala.sb.service;

import io.c12.bala.sb.dto.UserDto;
import io.c12.bala.sb.entity.UserEntity;
import io.c12.bala.sb.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    // Using real model mapper implementation
    private final ModelMapper modelMapper = new ModelMapper();

    private UserService userService;

    @BeforeEach
    void setUp() {
        this.userService = new UserServiceImpl(userRepository, modelMapper);
    }

    @Test
    void getUsers() {
        // given
        when(userRepository.findUserEntityByDeleteDateIsNull())
                .thenReturn(List.of(UserEntity.builder().firstName("John").lastName("Doe").uid("yUPSn34wcxVBtUo_pSisy").build()));

        // when
        var results = userService.getUsers();

        // then
        verify(userRepository, times(1)).findUserEntityByDeleteDateIsNull();      //  verify mock is called once
        assertEquals(1, results.size(), "Response result size have to be one");
        assertEquals("yUPSn34wcxVBtUo_pSisy", results.get(0).getUid(), "Response UUID have to match");
    }

    @Test
    void getUserById() {
        // given
        when(userRepository.findUserEntityByUidAndDeleteDateIsNull(anyString()))
                .thenReturn(UserEntity.builder().firstName("John").lastName("Doe").uid("0Cc3qbYUyzKR5yfGQ8HEx").build());

        // when
        var result = userService.getUserById("0Cc3qbYUyzKR5yfGQ8HEx");

        // then
        verify(userRepository, times(1)).findUserEntityByUidAndDeleteDateIsNull(anyString());
        assertEquals("0Cc3qbYUyzKR5yfGQ8HEx", result.getUid());
    }

    @Test
    void addUser() {
        // given
        when(userRepository.save(any(UserEntity.class)))
                .thenReturn(UserEntity.builder().firstName("John").lastName("Doe").uid("zxOTbgs1oVejtbhWhOrio").id(8754L).build());

        // when
        var result = userService.addUser(UserDto.builder().build());

        // then
        verify(userRepository, times(1)).save(any(UserEntity.class));
        assertEquals("zxOTbgs1oVejtbhWhOrio", result.getUid());
    }

    @Test
    void deleteUser() {
        // given
        when(userRepository.findUserEntityByUidAndDeleteDateIsNull(anyString()))
                .thenReturn(UserEntity.builder().firstName("John").lastName("Doe").uid("uNJZ0tB9d5pR3h2wepl9Q").build());

        // when
        userService.deleteUser("uNJZ0tB9d5pR3h2wepl9Q");

        // then
        InOrder inOrder = inOrder(userRepository);      // to verify the mock methods are called in order
        inOrder.verify(userRepository, times(1)).findUserEntityByUidAndDeleteDateIsNull("uNJZ0tB9d5pR3h2wepl9Q");
        inOrder.verify(userRepository, times(1)).save(any(UserEntity.class));
    }
}