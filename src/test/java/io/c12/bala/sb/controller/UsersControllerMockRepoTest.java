package io.c12.bala.sb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.c12.bala.sb.dto.UserDto;
import io.c12.bala.sb.entity.UserEntity;
import io.c12.bala.sb.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class UsersControllerMockRepoTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getAllUsers() throws Exception {
        // given
        when(userRepository.findUserEntityByDeleteDateIsNull())
                .thenReturn(List.of(UserEntity.builder().firstName("John").lastName("Doe").build()));

        // when
        var result = mockMvc.perform(get("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(print());
        result.andExpect(jsonPath("$.[0].firstName").isNotEmpty());
        result.andExpect(jsonPath("$.[0].firstName").value("John"));

        result.andExpect(jsonPath("$.[0].lastName").isNotEmpty());
        result.andExpect(jsonPath("$.[0].lastName").value("Doe"));

        verify(userRepository, times(1)).findUserEntityByDeleteDateIsNull();
    }

    @Test
    void getUserById() throws Exception {
        // given
        when(userRepository.findUserEntityByUidAndDeleteDateIsNull("3edcrgJyG0g48OzQNycEw"))
                .thenReturn(UserEntity.builder().uid("3edcrgJyG0g48OzQNycEw").firstName("John").lastName("Frank").build());

        // when
        var result = mockMvc.perform(get("/api/v1/users/{uid}", "3edcrgJyG0g48OzQNycEw")
                .contentType(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(print());
        result.andExpect(jsonPath("$.uid").isNotEmpty());
        result.andExpect(jsonPath("$.uid").value("3edcrgJyG0g48OzQNycEw"));

        result.andExpect(jsonPath("$.lastName").isNotEmpty());
        result.andExpect(jsonPath("$.lastName").value("Frank"));

        verify(userRepository, times(1)).findUserEntityByUidAndDeleteDateIsNull("3edcrgJyG0g48OzQNycEw");
    }

    @Test
    void addUser() throws Exception {
        // given
        when(userRepository.save(any(UserEntity.class)))
                .thenReturn(UserEntity.builder().uid("zEcYcO-xbuMAWkKPh0OSe").firstName("Jerry").lastName("Owens").userName("j_owens").build());
        UserDto userDto = UserDto.builder().firstName("Jerry").lastName("Owens").userName("j_owens").build();

        // when
        var result = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(userDto)));

        // then
        result.andExpect(status().isCreated()).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(print());
        result.andExpect(jsonPath("$.uid").isNotEmpty());
        result.andExpect(jsonPath("$.uid").value("zEcYcO-xbuMAWkKPh0OSe"));

        verify(userRepository, times(1)).save(any(UserEntity.class));
    }

    @Test
    void addUserMissingLastName() throws Exception {
        // given
        UserDto userDto = UserDto.builder().firstName("Jerry").userName("j_owens").build();

        // when
        var result = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(userDto)));

        // then
        result.andExpect(status().isBadRequest()).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(print());

        result.andExpect(jsonPath("$.apierror.message").value("Validation error"));
        result.andExpect(jsonPath("$.apierror.subErrors.[0].field").value("lastName"));
        result.andExpect(jsonPath("$.apierror.subErrors.[0].message").value("Last Name is required"));

        verify(userRepository, never()).save(any(UserEntity.class));
    }

    @Test
    void addUserMissingUserName() throws Exception {
        // given
        UserDto userDto = UserDto.builder().firstName("Jerry").lastName("Owens").build();

        // when
        var result = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(userDto)));

        // then
        result.andExpect(status().isBadRequest()).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(print());

        result.andExpect(jsonPath("$.apierror.message").value("Validation error"));
        result.andExpect(jsonPath("$.apierror.subErrors.[0].field").value("userName"));
        result.andExpect(jsonPath("$.apierror.subErrors.[0].message").value("User Name is required"));

        verify(userRepository, never()).save(any(UserEntity.class));
    }

    @Test
    void addUserShortUserName() throws Exception {
        // given
        UserDto userDto = UserDto.builder().firstName("Jerry").lastName("Owens").userName("jc").build();

        // when
        var result = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(userDto)));

        // then
        result.andExpect(status().isBadRequest()).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(print());

        result.andExpect(jsonPath("$.apierror.message").value("Validation error"));
        result.andExpect(jsonPath("$.apierror.subErrors.[0].field").value("userName"));
        result.andExpect(jsonPath("$.apierror.subErrors.[0].message").value("User Name have to be at lease 4 characters"));

        verify(userRepository, never()).save(any(UserEntity.class));
    }

    @Test
    void deleteUser() throws Exception {
        // given
        when(userRepository.findUserEntityByUidAndDeleteDateIsNull("9olxkJ0l2xfImL5v3bvbo"))
                .thenReturn(UserEntity.builder().uid("9olxkJ0l2xfImL5v3bvbo").firstName("John").lastName("Frank").build());
        when(userRepository.save(any(UserEntity.class))).thenReturn(UserEntity.builder().build());

        // when
        var result = mockMvc.perform(delete("/api/v1/users/{uid}", "9olxkJ0l2xfImL5v3bvbo"));

        // then
        result.andExpect(status().isNoContent());

        verify(userRepository, times(1)).findUserEntityByUidAndDeleteDateIsNull("9olxkJ0l2xfImL5v3bvbo");
        verify(userRepository, times(1)).save(any(UserEntity.class));
    }
}
