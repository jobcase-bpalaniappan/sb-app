package io.c12.bala.sb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.c12.bala.sb.dto.UserDto;
import io.c12.bala.sb.exception.UserNotFoundException;
import io.c12.bala.sb.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("User Controller Unit Testing with WebMvcTest")
@WebMvcTest(controllers = UsersController.class)
class UsersControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;

    @Test
    @DisplayName("Get all users in the DB who are active")
    void getAllUsers() throws Exception {
        // given
        when(userService.getUsers()).thenReturn(List.of(UserDto.builder().build()));

        // when
        var result = mockMvc.perform(get("/api/v1/users").contentType(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isOk()).andDo(print());
        verify(userService, times(1)).getUsers();
    }

    @Test
    @DisplayName("Get user by UID")
    void getUserById() throws Exception {
        // given
        when(userService.getUserById("aeB67EYBK6ZfZdSAKhzo-"))
            .thenReturn(UserDto.builder().uid("aeB67EYBK6ZfZdSAKhzo-").firstName("Jack").lastName("Reacher").userName("j_reach").build());

        // when
        var result = mockMvc.perform(get("/api/v1/users/{uid}", "aeB67EYBK6ZfZdSAKhzo-")
            .contentType(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isOk()).andDo(print());
        result.andExpect(jsonPath("$.uid").isNotEmpty());
        result.andExpect(jsonPath("$.uid").value("aeB67EYBK6ZfZdSAKhzo-"));

        result.andExpect(jsonPath("$.firstName").isNotEmpty());
        result.andExpect(jsonPath("$.firstName").value("Jack"));

        verify(userService, times(1)).getUserById(anyString());
    }

    @Test
    @DisplayName("Get user by UID when user is not found")
    void getUserByIdNotFound() throws Exception {
        // given
        when(userService.getUserById("aFOauo4PS9TMAESS3Zo7B"))
            .thenThrow(new UserNotFoundException("aFOauo4PS9TMAESS3Zo7B"));

        // when
        var result = mockMvc.perform(get("/api/v1/users/{uid}", "aFOauo4PS9TMAESS3Zo7B")
            .contentType(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isNotFound()).andDo(print());

        result.andExpect(jsonPath("$.apierror.message").isNotEmpty());
        result.andExpect(jsonPath("$.apierror.message").value("User Not Found"));

        result.andExpect(jsonPath("$.apierror.debugMessage").isNotEmpty());
        result.andExpect(jsonPath("$.apierror.debugMessage").value("User Not found for uid - aFOauo4PS9TMAESS3Zo7B"));

        verify(userService, times(1)).getUserById(anyString());
    }

    @Test
    @DisplayName("Add User to UserController")
    void addUser() throws Exception {
        // given
        UserDto inUserDto = UserDto.builder().userName("jchild").firstName("Julia").lastName("Child").build();
        UserDto outUserDto = UserDto.builder().userName("jchild").firstName("Julia").lastName("Child").uid("qNqKKYzH0SNaXJz6b9JRw").build();
        when(userService.addUser(any(UserDto.class))).thenReturn(outUserDto);

        // when
        var result = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(inUserDto)));

        // then
        result.andExpect(status().isCreated()).andDo(print());
        result.andExpect(jsonPath("$.uid").isNotEmpty());
        result.andExpect(jsonPath("$.uid").value("qNqKKYzH0SNaXJz6b9JRw"));

        verify(userService, atLeastOnce()).addUser(any(UserDto.class));
    }

    @Test
    @DisplayName("Add User throws validation error for missing Last Name")
    void addUserMissingLastName() throws Exception {
        // given
        UserDto inUserDto = UserDto.builder().userName("jchild").firstName("Julia").build();

        // when
        var result = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(inUserDto)));

        // then
        result.andExpect(status().isBadRequest()).andDo(print());
        result.andExpect(jsonPath("$.apierror.message").value("Validation error"));
        result.andExpect(jsonPath("$.apierror.subErrors.[0].field").value("lastName"));
        result.andExpect(jsonPath("$.apierror.subErrors.[0].message").value("Last Name is required"));

        verify(userService, never()).addUser(any(UserDto.class));
    }

    @Test
    @DisplayName("Add User throws validation error for Missing user name")
    void addUserMissingUserName() throws Exception {
        // given
        UserDto inUserDto = UserDto.builder().firstName("Julia").lastName("Child").build();

        // when
        var result = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(inUserDto)));

        // then
        result.andExpect(status().isBadRequest()).andDo(print());
        result.andExpect(jsonPath("$.apierror.message").value("Validation error"));
        result.andExpect(jsonPath("$.apierror.subErrors.[0].field").value("userName"));
        result.andExpect(jsonPath("$.apierror.subErrors.[0].message").value("User Name is required"));

        verify(userService, never()).addUser(any(UserDto.class));
    }

    @Test
    @DisplayName("Add User throws validation error for User Name is short")
    void addUserUserNameIsTooShort() throws Exception {
        // given
        UserDto inUserDto = UserDto.builder().firstName("Julia").lastName("Child").userName("jc").build();

        // when
        var result = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(inUserDto)));

        // then
        result.andExpect(status().isBadRequest()).andDo(print());
        result.andExpect(jsonPath("$.apierror.message").value("Validation error"));
        result.andExpect(jsonPath("$.apierror.subErrors.[0].field").value("userName"));
        result.andExpect(jsonPath("$.apierror.subErrors.[0].message").value("User Name have to be at lease 4 characters"));

        verify(userService, never()).addUser(any(UserDto.class));
    }

    @Test
    @DisplayName("Soft delete user from system using UID")
    void deleteUser() throws Exception {
        // when
        var result = mockMvc.perform(delete("/api/v1/users/{uid}", "yTPY0-i836CYbqeAeuZBn")
            .contentType(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isNoContent());

        verify(userService, times(1)).deleteUser("yTPY0-i836CYbqeAeuZBn");
    }
}
