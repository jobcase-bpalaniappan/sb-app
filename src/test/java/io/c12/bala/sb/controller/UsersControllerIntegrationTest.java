package io.c12.bala.sb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import io.c12.bala.sb.dto.UserDto;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration testing with H2 in-memory DB and Springboot testing.
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UsersControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @Order(2)
    void getAllUsers() throws Exception {
        // when
        var result = mockMvc.perform(get("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(print());
        result.andExpect(jsonPath("$.[0].firstName").isNotEmpty());
        result.andExpect(jsonPath("$.[0].firstName").value("John"));
    }

    @Test
    @Order(3)
    void getUserById() throws Exception {
        // when
        var result = mockMvc.perform(get("/api/v1/users/{uid}", getUidFromGetAllUsers())
                .contentType(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(print());
        result.andExpect(jsonPath("$.firstName").isNotEmpty());
    }

    @Test
    @Order(1)
    void addUser() throws Exception {
        // given
        UserDto userDto = UserDto.builder().firstName("John").lastName("Doe").userName("j_doe").build();

        // when
        var result = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userDto)));

        // then
        result.andExpect(status().isCreated()).andDo(print());
        result.andExpect(jsonPath("$.firstName").isNotEmpty());
        result.andExpect(jsonPath("$.firstName").value("John"));
    }

    @Test
    @Order(4)
    void deleteUser() throws Exception {
        // when
        var result = mockMvc.perform(delete("/api/v1/users/{uid}", getUidFromGetAllUsers())
                .contentType(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isNoContent()).andDo(print());
    }

    /**
     * Get UID of already created users.
     *
     * @return valid UID of an user.
     * @throws Exception on any failure.
     */
    private String getUidFromGetAllUsers() throws Exception {
        var result = mockMvc.perform(get("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON));
        return JsonPath.read(result.andReturn().getResponse().getContentAsString(), "$.[0].uid");
    }
}
