CREATE TABLE IF NOT EXISTS public.users
(
    id bigint NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    first_name varchar(50),
    last_name varchar(50) not null,
    uid varchar(40) not null unique,
    user_name varchar(100) not null,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE,
    deleted_at TIMESTAMP WITH TIME ZONE
);
