package io.c12.bala.sb.controller;

import io.c12.bala.sb.dto.UserDto;
import io.c12.bala.sb.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
@Validated
public class UsersController {

    private final UserService userService;

    // GET All
    @GetMapping
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return ResponseEntity.ok(userService.getUsers());
    }

    // GET one
    @GetMapping("/{uid}")
    public ResponseEntity<UserDto> getUserById(@PathVariable String uid) {
        return ResponseEntity.ok(userService.getUserById(uid));
    }

    // POST / Create
    @PostMapping
    public ResponseEntity<UserDto> addUser(@Valid @RequestBody UserDto userDto) {
        return new ResponseEntity<>(userService.addUser(userDto), HttpStatus.CREATED);
    }

    // PUT / Update
    @PutMapping("/{uid}")
    public ResponseEntity<UserDto> updateUser(@Valid @RequestBody UserDto userDto) {
        return null;
    }

    // PATCH / Update
    @PatchMapping("/{uid}")
    public ResponseEntity<UserDto> updateUserPartial(@RequestBody UserDto userDto) {
        return null;
    }

    // DELETE / Remove
    @DeleteMapping("/{uid}")
    public ResponseEntity<?> deleteUser(@PathVariable String uid) {
        userService.deleteUser(uid);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
