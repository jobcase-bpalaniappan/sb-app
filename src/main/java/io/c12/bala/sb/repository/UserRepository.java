package io.c12.bala.sb.repository;

import io.c12.bala.sb.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findUserEntityByUidAndDeleteDateIsNull(String uid);

    List<UserEntity> findUserEntityByDeleteDateIsNull();

    void deleteUserEntityByUid(String uid);

}
