package io.c12.bala.sb.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "USERS", schema = "public")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uid", length = 40)
    private String uid;

    @Column(name = "first_name", length = 50)
    private String firstName;

    @Column(name = "last_name", length = 50, nullable = false)
    private String lastName;

    @Column(name = "user_name", length = 100, nullable = false, unique = true)
    private String userName;

    @Column(name = "created_at", columnDefinition = "TIMESTAMP")
    private LocalDateTime createDate;

    @Column(name = "updated_at", columnDefinition = "TIMESTAMP")
    private LocalDateTime updateDate;

    @Column(name = "deleted_at", columnDefinition = "TIMESTAMP")
    private LocalDateTime deleteDate;
}
