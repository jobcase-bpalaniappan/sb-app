package io.c12.bala.sb.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String uid;
    private String firstName;
    @NotEmpty(message = "Last Name is required")
    private String lastName;
    @NotBlank(message = "User Name is required")
    @Size(min = 4, message = "User Name have to be at lease 4 characters")
    private String userName;
}
