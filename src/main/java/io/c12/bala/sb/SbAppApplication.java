package io.c12.bala.sb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbAppApplication.class, args);
    }

}
