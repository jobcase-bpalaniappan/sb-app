package io.c12.bala.sb.exception;

import java.util.function.Supplier;

public class UserNotFoundException extends RuntimeException implements Supplier<UserNotFoundException> {

    public UserNotFoundException(String uid) {
        super("User Not found for uid - " + uid);
    }

    @Override
    public UserNotFoundException get() {
        return this;
    }
}
