package io.c12.bala.sb.service;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import io.c12.bala.sb.dto.UserDto;
import io.c12.bala.sb.entity.UserEntity;
import io.c12.bala.sb.exception.UserNotFoundException;
import io.c12.bala.sb.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final ModelMapper modelMapper;

    /**
     * @return List of User DTO objects
     */
    @Override
    public List<UserDto> getUsers() {
        List<UserEntity> userEntityList = userRepository.findUserEntityByDeleteDateIsNull();
        return userEntityList.stream().map(x -> modelMapper.map(x, UserDto.class)).toList();
    }

    /**
     * @param uid - Unique UUID of user
     * @return User id found by unique ID
     */
    @Override
    public UserDto getUserById(String uid) {
        UserEntity userEntity = userRepository.findUserEntityByUidAndDeleteDateIsNull(uid);
        if (userEntity == null) {
            throw new UserNotFoundException(uid);
        }
        return modelMapper.map(userEntity, UserDto.class);
    }

    /**
     * @param userDto to be added
     * @return persisted user
     */
    @Override
    public UserDto addUser(UserDto userDto) {
        userDto.setUid(NanoIdUtils.randomNanoId());
        UserEntity userEntity = modelMapper.map(userDto, UserEntity.class);
        userEntity.setCreateDate(LocalDateTime.now());
        return modelMapper.map(userRepository.save(userEntity), UserDto.class);
    }

    /**
     * @param uid - to be deleted
     */
    @Override
    public void deleteUser(String uid) {
        UserEntity userEntity = userRepository.findUserEntityByUidAndDeleteDateIsNull(uid);
        userEntity.setDeleteDate(LocalDateTime.now());
        userRepository.save(userEntity);
    }
}
