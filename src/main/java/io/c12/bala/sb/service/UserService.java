package io.c12.bala.sb.service;

import io.c12.bala.sb.dto.UserDto;

import java.util.List;

public interface UserService {

    List<UserDto> getUsers();
    UserDto getUserById(String uid);
    UserDto addUser(UserDto userDto);
    void deleteUser(String uid);

}
